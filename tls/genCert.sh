#!/bin/bash
CA_DIR=ca
CA_CER=$CA_DIR/cacer.pem
CA_KEY=$CA_DIR/cakey.pem
CA_DAY=1000
CA_SRL=$CA_DIR/cacer.srl
SERVER_DIR=certs
SERVER_KEY=$SERVER_DIR/serverkey.pem
SERVER_CSR=$SERVER_DIR/servercer.csr
SERVER_CER=$SERVER_DIR/servercer.pem
SERVER_DAY=60
SERVER_POUND_CER=$SERVER_DIR/server_pound_cer.pem
CNF_FILE=server.conf

function usage {
    echo 'Use ca, cer'
}

function gen_ca {
    echo 'Generating CA cert'
    if [ ! -d $CA_DIR ]; then
        mkdir $CA_DIR
    fi
    # Create CA cert
    openssl req -x509 -newkey rsa -sha256 -nodes -days 1000 -out $CA_CER -keyout $CA_KEY -subj "/CN=$USER"
    echo 00 > $CA_SRL
}

function gen_cer {
    echo 'Generating cert'

    if [ ! -d $SERVER_DIR ]; then
        mkdir $SERVER_DIR
    fi

    # Create CSR
    openssl req -newkey rsa:2048 -sha256 -nodes -keyout $SERVER_KEY -out $SERVER_CSR -config $CNF_FILE;
    
    if [ ! -f $CA_CER ]; then
        echo 'No CA cert, generating...'
        gen_ca
    fi

    # Sign with CA cert
    openssl x509 -req -in $SERVER_CSR -sha256 -CA $CA_CER -CAkey $CA_KEY -days $SERVER_DAY -out $SERVER_CER -CAserial $CA_SRL \
    -extfile $CNF_FILE -extensions v3_req
    
    # Concat cert/key
    cat $SERVER_CER $SERVER_KEY > $SERVER_POUND_CER
}

case $1 in
    ca)
        gen_ca
        ;;
    cer)
        gen_cer
        ;;
    *)
        usage
        ;;
esac
